#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import re
import time

from helpers import const


def random_wait(long=False, ultraShort=False):
    if long:
        time.sleep(random.uniform(300, 750))
    elif ultraShort:
        time.sleep(random.uniform(5, 10))
    else:
        time.sleep(random.uniform(25, 45))


def get_product_property(product_html, config):
    if const.SELECTOR in config and ('' == config[const.SELECTOR] or not product_html.select(config[const.SELECTOR])):
        return None
    return get_value_from_attribute(
        product_html.select(config[const.SELECTOR])[config[const.ITERATOR] if const.ITERATOR in config else 0]
        if const.SELECTOR in config else product_html,
        config[const.ATTRIBUTE] if const.ATTRIBUTE in config else const.ATTRIBUTE_TEXT
    )


def get_value_from_attribute(html_tag, attribute):
    if isinstance(attribute, list):
        if const.STYLE == attribute[0]:
            return html_tag[const.STYLE][attribute[1]:attribute[2]]
        try:
            return get_value_from_attribute(html_tag, attribute[0])
        except KeyError:
            return get_value_from_attribute(html_tag, attribute[1])
    if const.ATTRIBUTE_TEXT == attribute:
        return html_tag.text.strip()
    return html_tag[attribute]


def get_price_regex(price_text):
    if not price_text:
        return None

    if price_text.find('|') != -1:
        price_text = price_text[price_text.find('|'):]

    price_text = price_text.encode('ascii', 'ignore').decode('ascii').replace(' ', '')
    if '.' not in price_text and ',' not in price_text:
        price_text += '.00'
    return re.findall('\d*,\d+|\d+', price_text)[0].replace(',', '.')


