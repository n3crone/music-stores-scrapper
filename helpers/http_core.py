import time
from contextlib import closing
from http import HTTPStatus

from requests import get, post, RequestException

SLEEP_TIMEOUT = 3

MAX_TRIES = 5


def simple_get(url, tries=0):
    while True:
        try:
            with closing(get(url, stream=True, timeout=10)) as resp:
                if is_good_response(resp):
                    return resp.content
                continue

        except RequestException as e:
            time.sleep(SLEEP_TIMEOUT)
            if tries == MAX_TRIES:
                raise e
            return simple_get(url, tries + 1)


def simple_post(url, data, tries=0):
    while True:
        try:
            with closing(post(url, data=data, timeout=10)) as resp:
                if HTTPStatus.OK == resp.status_code:
                    return resp.content

        except RequestException as e:
            time.sleep(SLEEP_TIMEOUT)
            if tries == MAX_TRIES:
                raise e
            return simple_post(url, data, tries + 1)


def is_good_response(resp):
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)
