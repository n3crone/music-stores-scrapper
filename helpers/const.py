SHOP_NAME = 'shop_name'
BASE_REQUEST_URL = 'base_request_url'
SEARCH_URLS = 'search_urls'
PAYLOAD = 'payload'
PAGE = 'page'
ROW = 'row'
LIST = 'list'
NAME = 'name'
PRICE_NORMAL = 'price'
PRICE_DISCOUNT = 'discount_price'
HREF = 'href'
DISCOUNT = 'discount'
QUALITY = 'quality'
ADDITIONAL_INFO = 'additional_info'
IMAGE = 'image'
ATTRIBUTE_TEXT = 'text'
ATTRIBUTE = 'attribute'
ITERATOR = 'iterator'
PREFIX = 'prefix'
PARENT = 'parent'
SELECTOR = 'selector'
STYLE = 'style'
