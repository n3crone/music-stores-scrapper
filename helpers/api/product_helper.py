# coding=utf-8
import json
from http import HTTPStatus

from exceptions.ProductRequestException import ProductRequestException
from helpers import json_helper
from helpers.api import request_helper

BASE_ENDPOINT = 'products'


def handle_product(product):
    product_id = find_product(product)
    if product_id:
        return update_product(product, product_id)

    return create_product(product)


def find_product(product):
    product_response = request_helper.get(
        '{}?{}={}&{}={}'.format(BASE_ENDPOINT, 'baseInfo', product.baseInfo, 'link', product.link)
    )

    if product_response.content != b'[]':
        return json.loads(product_response.content)[0]['id']


def create_product(product):
    product_response = request_helper.upsert(BASE_ENDPOINT, json_helper.dump(product))

    if HTTPStatus.CREATED != product_response.status_code:
        raise ProductRequestException('Product creation failed!')

    return product_response.content


def update_product(product, product_id):
    if product.image:
        del product.image

    product_response = request_helper.upsert('{}/{}'.format(BASE_ENDPOINT, product_id),
                                             json_helper.dump(product), 'put')

    if HTTPStatus.OK != product_response.status_code:
        raise ProductRequestException('Product update failed!')

    return product_response.content
