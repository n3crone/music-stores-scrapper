# coding=utf-8
import os
from datetime import datetime, timedelta
from http import HTTPStatus

import jwt
import requests

from exceptions.FileUploadException import FileUploadException
from exceptions.UnauthorizedException import UnauthorizedException
from helpers import json_helper
from helpers.api import apiConfig

AUTH_PATH = 'authentication_token'


def get(url_param):
    return make_request(url_param, 'get')


def upsert(url_param, payload, method='post'):
    return make_request(url_param, method, payload)


def upload(url_param, file):
    response = make_request(url_param, 'post', headers={}, file=file)

    if HTTPStatus.CREATED != response.status_code:
        raise FileUploadException('File upload failed! {}'.format(json_helper.dump(file)))

    return response


def make_request(url_param, method, payload=None, headers=None, file=None):
    if headers is None:
        headers = apiConfig.headers

    if url_param != AUTH_PATH:
        headers.update(handle_auth_header())

    response = requests.request(method, '{}{}'.format(apiConfig.baseRequestUrl, url_param),
                                headers=headers, data=payload, verify=apiConfig.sslVerify, files=file)

    if HTTPStatus.UNAUTHORIZED == response.status_code:
        raise UnauthorizedException('Unauthorized! Check JWT token in apiConfig.py!')

    return response


def handle_auth_header():
    if not os.path.exists('tmp/jwttoken'):
        login()

    f = open('tmp/jwttoken', 'r')
    token = f.read()
    f.close()

    if datetime.utcnow() - timedelta(minutes=5) > datetime.fromtimestamp(jwt.decode(token, verify=False)['exp']):
        login()

    return {'Authorization': 'Bearer {}'.format(token)}


def login():
    token = make_request(AUTH_PATH, 'post', payload=json_helper.dump(apiConfig.authHeader)).json()['token']
    f = open('tmp/jwttoken', 'w')
    f.write(token)
    f.close()
