# coding=utf-8
import json
from http import HTTPStatus

from bs4 import BeautifulSoup

from exceptions.ShopRequestException import ShopRequestException
from helpers import http_core as crawler, json_helper
from helpers.api import image_helper, request_helper

BASE_ENDPOINT = 'shops'


def get_or_create_shop(shop_name, base_request_url):
    shop_response = request_helper.get('{}?name={}'.format(BASE_ENDPOINT, shop_name))

    if shop_response.content == b'[]':
        shop_response = create_shop(shop_name, base_request_url)

    content = json.loads(shop_response.content)
    if isinstance(content, list):
        content = content[0]

    return 'shops/' + content['id'].__str__()


def create_shop(shop_name, base_request_url):
    icon_link = handle_favicon(base_request_url)
    response = image_helper.handle_image(shop_name, icon_link['href'])
    shop_response = request_helper.upsert(BASE_ENDPOINT, json_helper.dump({'name': shop_name, 'favicon': response}))

    if HTTPStatus.CREATED != shop_response.status_code:
        raise ShopRequestException('Shop creation failed. Response invalid!')

    return shop_response


def handle_favicon(base_request_url):
    soup = BeautifulSoup(crawler.simple_get(base_request_url), 'html.parser')
    icon_link = soup.find('link', rel='shortcut icon')
    if not icon_link:
        icon_link = soup.find('link', rel='icon')
    if not icon_link:
        icon_link = {'href': base_request_url + '/favicon.ico'}
    if icon_link['href'].startswith('//stat-m1.ms-online.pl'):
        icon_link['href'] = 'https:' + icon_link['href']
    if not icon_link['href'].startswith('http'):
        if icon_link['href'].startswith('/'):
            icon_link['href'] = base_request_url[:-1] + icon_link['href']
        else:
            icon_link['href'] = base_request_url + icon_link['href']

    return icon_link
