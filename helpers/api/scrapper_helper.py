# coding=utf-8
import datetime
from http import HTTPStatus

from exceptions.ScrapperRequestException import ScrapperRequestException
from helpers import json_helper
from helpers.api import request_helper

BASE_ENDPOINT = 'scrappers'


def start_scrapper(shop):
    scrapper_response = request_helper.upsert(BASE_ENDPOINT, json_helper.dump({'shop': shop}))

    if HTTPStatus.CREATED != scrapper_response.status_code:
        raise ScrapperRequestException('Unable to create scrapper.')

    return scrapper_response.json()['id']


def update_scrapper_page(page, scrapper_id):
    scrapper_response = request_helper.upsert(
        '{}/{}'.format(BASE_ENDPOINT, scrapper_id.__str__()),
        json_helper.dump({'page': page}),
        'put'
    )

    if HTTPStatus.OK != scrapper_response.status_code:
        raise ScrapperRequestException('Unable to update scrapper.')

    return scrapper_response.json()['id']


def end_scrapper(scrapper_id):
    scrapper_response = request_helper.upsert(
        '{}/{}'.format(BASE_ENDPOINT, scrapper_id.__str__()),
        json_helper.dump({'datetimeEnd': datetime.datetime.now()}),
        'put'
    )

    if HTTPStatus.OK != scrapper_response.status_code:
        raise ScrapperRequestException('Unable to end scrapper.')

    return scrapper_response
