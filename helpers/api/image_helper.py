# coding=utf-8
import json
import uuid

import requests
from requests.exceptions import MissingSchema

from helpers.api import request_helper

BASE_ENDPOINT = 'media_objects'


def handle_image(shop_name, img, url=None):
    create_image_file(img, shop_name, url)
    multipart_form_data = {'file': (str(uuid.uuid4()) + '.jpg', open('tmp/images/' + shop_name + '.jpg', 'rb'))}
    response = request_helper.upload(BASE_ENDPOINT, multipart_form_data)

    return json.loads(response.content)['@id']


def create_image_file(img, shop_name, url):
    f = open('tmp/images/' + shop_name + '.jpg', 'wb')
    if url:
        img = url + img['data-src']
    try:
        f.write(requests.get(img).content)
    except MissingSchema:
        f.write(requests.get('https:' + img).content)
    f.close()
