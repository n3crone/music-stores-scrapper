import datetime
import json


def dump(dump_object, indent=None):
    return json.dumps(dump_object, indent=indent, default=dict_dump_strategy)


def dict_dump_strategy(value):
    if isinstance(value, datetime.date):
        return value.strftime("%Y-%m-%d %H:%M:%S")
    else:
        return value.__dict__
