from helpers import const

CONFIG = {
    const.SHOP_NAME: 'SneakerStudio',
    const.BASE_REQUEST_URL: 'https://sneakerstudio.pl/',
    const.SEARCH_URLS: [
        'https://sneakerstudio.pl/pol_m_-665.html?counter=%s',
        'https://sneakerstudio.pl/pol_m_MESKIE_BUTY-MESKIE_Sneakersy-meskie-2475.html?counter=%s'
    ],
    const.PAGE: 0,
    const.ROW: 'div#search div.product_wrapper',
    const.NAME: {const.SELECTOR: 'a.product-name'},
    const.PRICE_NORMAL: {const.SELECTOR: 'div.product_prices span.max-price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: 'div.product_prices span.price'},
    const.HREF: {
        const.SELECTOR: 'a.product-name',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: ''},
    const.IMAGE: {
        const.SELECTOR: 'img',
        const.ATTRIBUTE: 'data-lazy',
        const.PREFIX: 'https://sneakerstudio.pl/'
    },
}
