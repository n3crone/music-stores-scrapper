from helpers import const

CONFIG = {
    const.SHOP_NAME: 'ButyJana',
    const.BASE_REQUEST_URL: 'https://www.butyjana.pl/',
    const.SEARCH_URLS: [
        'https://www.butyjana.pl/obuwie-meskie.html?p=%s',
        'https://www.butyjana.pl/outlet/l/meskie.html?p=%s'
    ],
    const.PAGE: 1,
    const.ROW: 'div.category-products li.item',
    const.NAME: {const.SELECTOR: 'h3'},
    const.PRICE_NORMAL: {const.SELECTOR: 'div.priceBuy div.promo del'},
    const.PRICE_DISCOUNT: {const.SELECTOR: 'div.priceBuy div.price'},
    const.HREF: {
        const.SELECTOR: 'h3 a',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: 'ul.configurable-attributes'},
    const.IMAGE: {
        const.SELECTOR: 'img.product__images__img',
        const.ATTRIBUTE: 'src'
    },
}
