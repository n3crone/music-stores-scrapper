from helpers import const

CONFIG = {
    const.SHOP_NAME: 'DwanascieCali',
    const.BASE_REQUEST_URL: 'http://dwanasciecali.pl/shopp/',
    const.SEARCH_URLS: [
        'http://www.dwanasciecali.pl/shopp/index.php?page=%s&ipp=25&produkt=pk&id=12&lan=pl'
    ],
    const.PAGE: 1,
    const.ROW: 'div.produktpoziom',
    const.NAME: {const.SELECTOR: 'div.zespolpoziom'},
    const.PRICE_NORMAL: {const.SELECTOR: 'div.cenapoziom'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.HREF: {
        const.SELECTOR: 'div.okladkapoziom a',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: 'div.albumpoziom'},
    const.IMAGE: {
        const.SELECTOR: 'div.okladkapoziom img',
        const.ATTRIBUTE: 'src',
        const.PREFIX: 'http://dwanasciecali.pl/'
    },
}
