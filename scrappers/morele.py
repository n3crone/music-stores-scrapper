from helpers import const

CONFIG = {
    const.SHOP_NAME: 'Morele',
    const.BASE_REQUEST_URL: 'https://www.morele.net/?%d',
    const.SEARCH_URLS: [
        'https://www.morele.net/api/widget/promotion_products/'
    ],
    const.PAYLOAD: '{"customClass":"","slider":false,"promotionId":null,"minProducts":1,"limit":24,'
                   '"showInstalmentInfo":true,"staticInstalmentPeriod":10,"widgetType":2,"showFilters":true,'
                   '"showPagination":true,"isOutlet":true,"hideLastRow":false,"catgroups":[],"categories":[],'
                   '"page":%s} ',
    const.PAGE: 1,
    const.ROW: 'div.section-block div.owl-item',
    const.NAME: {const.SELECTOR: 'div.product-slider-name'},
    const.PRICE_NORMAL: {const.SELECTOR: 'div.product-slider-price span'},
    const.PRICE_DISCOUNT: {
        const.SELECTOR: 'div.product-slider-price span',
        const.ITERATOR: 1
    },
    const.HREF: {
        const.SELECTOR: 'a.link-bottom',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: 'div.state-dictionary'},
    const.ADDITIONAL_INFO: {const.SELECTOR: ''},
    const.IMAGE: {
        const.SELECTOR: 'div.prod-img',
        const.ATTRIBUTE: [const.STYLE, 22, -1]
    },
}
