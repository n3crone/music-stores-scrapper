from helpers import const

CONFIG = {
    const.SHOP_NAME: 'RTV Euro AGD',
    const.BASE_REQUEST_URL: 'https://www.euro.com.pl/',
    const.SEARCH_URLS: [
        'https://www.euro.com.pl/search,stan-outlet-doskonaly:outlet-dobry,d10,strona-%s.bhtml?keyword=outlet&searchType=tag'
    ],
    const.PAGE: 1,
    const.ROW: 'div#products div.product-row',
    const.NAME: {const.SELECTOR: 'h2.product-name'},
    const.PRICE_NORMAL: {const.SELECTOR: 'span.tab-price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: 'div.product-sales-category-outlet div.price-normal'},
    const.HREF: {
        const.SELECTOR: 'h2.product-name a',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {
        const.SELECTOR: 'a.outlet-quality-category',
        const.ATTRIBUTE: 'data-outlet-quality'
    },
    const.ADDITIONAL_INFO: {const.SELECTOR: ''},
    const.IMAGE: {
        const.SELECTOR: 'a.photo-hover img',
        const.ATTRIBUTE: ['data-original', 'src']
    },
}
