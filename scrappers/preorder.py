from helpers import const

CONFIG = {
    const.SHOP_NAME: 'Preorder',
    const.BASE_REQUEST_URL: 'https://preorder.pl',
    const.SEARCH_URLS: [
        'https://preorder.pl/muzyka/page:%s'
    ],
    const.PAGE: 1,
    const.ROW: 'ul#MainProductList li.product-box',
    const.NAME: {const.SELECTOR: 'span.product-producer'},
    const.PRICE_NORMAL: {const.SELECTOR: 'span.price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.HREF: {
        const.SELECTOR: 'a.product-name',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: 'a.product-name'},
    const.IMAGE: {
        const.SELECTOR: 'img.lazy',
        const.ATTRIBUTE: ['data-original']
    },
}
