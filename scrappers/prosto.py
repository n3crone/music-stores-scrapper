from helpers import const

CONFIG = {
    const.SHOP_NAME: 'Prosto',
    const.BASE_REQUEST_URL: 'https://prosto.pl/',
    const.SEARCH_URLS: [
        'https://prosto.pl/shop/category/muzyka/CD/?%s'
    ],
    const.PAGE: 1,
    const.ROW: 'div.products div.product',
    const.NAME: {const.SELECTOR: 'div.artist'},
    const.PRICE_NORMAL: {const.SELECTOR: 'div.price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.HREF: {
        const.SELECTOR: 'div.name a',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: 'div.name'},
    const.IMAGE: {
        const.SELECTOR: 'a.image img',
        const.ATTRIBUTE: 'src'
    },
}
