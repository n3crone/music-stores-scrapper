from helpers import const

CONFIG = {
    const.SHOP_NAME: 'MediaMarkt',
    const.BASE_REQUEST_URL: 'https://mediamarkt.pl/',
    const.SEARCH_URLS: [
        'https://mediamarkt.pl/outlet?limit=100&page=%s'
    ],
    const.PAGE: 1,
    const.ROW: 'div.b-mainContent div.m-offerBox',
    const.NAME: {const.SELECTOR: 'a.js-product-name'},
    const.PRICE_NORMAL: {const.SELECTOR: 'span.m-priceBox_oldPrice'},
    const.PRICE_DISCOUNT: {
        const.SELECTOR: 'a.js-product-name',
        const.ATTRIBUTE: 'data-offer-price'
    },
    const.HREF: {
        const.SELECTOR: 'a.js-product-name',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: 'div.outlet_description'},
    const.ADDITIONAL_INFO: {const.SELECTOR: ''},
    const.IMAGE: {
        const.SELECTOR: 'div.m-offerBox_box img',
        const.ATTRIBUTE: 'data-target'
    },
}
