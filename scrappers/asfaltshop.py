from helpers import const

CONFIG = {
    const.SHOP_NAME: 'AsfaltShop',
    const.BASE_REQUEST_URL: 'http://asfaltshop.pl/',
    const.SEARCH_URLS: [
        'http://asfaltshop.pl/category/1/?page=%s'
    ],
    const.PAGE: 1,
    const.ROW: 'div.kafelki a',
    const.NAME: {const.SELECTOR: 'div.title div'},
    const.ADDITIONAL_INFO: {
        const.SELECTOR: 'div.title div',
        const.ITERATOR: 1
    },
    const.PRICE_NORMAL: {const.SELECTOR: 'div.pricing'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.QUALITY: {const.SELECTOR: ''},
    const.HREF: {const.ATTRIBUTE: 'href'},
    const.IMAGE: {
        const.SELECTOR: 'img.productThumb',
        const.ATTRIBUTE: 'data-original'
    },
}
