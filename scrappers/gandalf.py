from helpers import const

CONFIG = {
    const.SHOP_NAME: 'Gandalf',
    const.BASE_REQUEST_URL: 'https://www.gandalf.com.pl/',
    const.SEARCH_URLS: [
        'https://www.gandalf.com.pl/muzyka/hip-hoprap/%s'
    ],
    const.PAGE: 0,
    const.ROW: 'div#pli div.prod',
    const.NAME: {const.SELECTOR: 'p.person'},
    const.PRICE_NORMAL: {const.SELECTOR: 'span.new_price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.HREF: {
        const.SELECTOR: 'p a',
        const.ATTRIBUTE: 'href',
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {
        const.SELECTOR: 'p a',
        const.ATTRIBUTE: 'title',
    },
    const.IMAGE: {
        const.SELECTOR: 'div.pimage img',
        const.ATTRIBUTE: 'src',
        const.PREFIX: 'https://www.gandalf.com.pl/'
    },
}
