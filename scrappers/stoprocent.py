from helpers import const

CONFIG = {
    const.SHOP_NAME: 'StoProcent',
    const.BASE_REQUEST_URL: 'https://stoprocent.com/',
    const.SEARCH_URLS: [
        'https://www.stoprocent.com/pl/muzyka.html?ajax=1&p=%s'
    ],
    const.PAGE: 1,
    const.ROW: 'article',
    const.NAME: {
        const.SELECTOR: 'a.product-name',
        const.ATTRIBUTE: 'title'
    },
    const.PRICE_NORMAL: {const.SELECTOR: 'span.price'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.HREF: {
        const.SELECTOR: 'a.product-name',
        const.ATTRIBUTE: 'href'
    },
    const.QUALITY: {const.SELECTOR: ''},
    const.ADDITIONAL_INFO: {const.SELECTOR: ''},
    const.IMAGE: {
        const.SELECTOR: 'div.p-photo img',
        const.ATTRIBUTE: 'src'
    },
}
