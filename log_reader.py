import subprocess
import logging
import sys

def main():
    print('Opening scrapper log file...')
    open('tmp/logs/logs.log', 'a').close()
    subprocess.call('tail -F tmp/logs/logs.log', shell=True)


if __name__ == "__main__":
    main()
