import getopt
import ssl
import sys
import urllib3

from ProductScrapper import ProductScrapper
from scrappers import mediamarkt, stoprocent, dwanasciecali, rtveuroagd, sneakerstudio, asfaltshop, butyjana, \
    morele, prosto

configs = {
    'rtv': rtveuroagd.CONFIG,
    'media': mediamarkt.CONFIG,
    'morele': morele.CONFIG,
    'asfalt': asfaltshop.CONFIG,
    'butyjana': butyjana.CONFIG,
    'dwanascie': dwanasciecali.CONFIG,
    # 'gandalf': gandalf.CONFIG, //TODO: New FE
    'prosto': prosto.CONFIG,
    'sneaker': sneakerstudio.CONFIG,
    'stoprocent': stoprocent.CONFIG,
    # 'preorder': preorder.CONFIG, //TODO: Second request handler
}


def main(argv):
    # sentry_sdk.init("https://f56179ad7fe5427cb856722368be8891@sentry.io/2134463")
    disable_ssl_warnings()

    scrapper = ''
    opts, args = getopt.getopt(argv, 's:')
    if not opts:
        print_help()
    for opt, arg in opts:
        if arg not in configs.keys():
            print_help()
        scrapper = arg
    scrapper = ProductScrapper(configs[scrapper])
    scrapper.start()


def disable_ssl_warnings():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context


def print_help():
    print('scrapper.py -s <scrapper>')
    print(configs.keys())
    sys.exit()


if __name__ == "__main__":
    main(sys.argv[1:])
