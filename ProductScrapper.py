#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging
import sys
from datetime import datetime

from bs4 import BeautifulSoup
from requests import Timeout

from Product import Product
from exceptions.UnauthorizedException import UnauthorizedException
from helpers import fetch_helper, const, http_core as crawler, json_helper
from helpers.api import shop_helper, product_helper, scrapper_helper, image_helper


class ProductScrapper:
    scrapper_id = ''
    shop_id = ''
    html = ''
    search_url = ''
    product_html = ''
    products_overall = []
    products_scrapped = []
    last_page_products = []

    def __init__(self, config):
        self.shop_name = config[const.SHOP_NAME]
        self.base_request_url = config[const.BASE_REQUEST_URL]
        self.page = config[const.PAGE]
        self.search_urls = config[const.SEARCH_URLS]
        self.config = config
        logging.basicConfig(filename='tmp/logs/logs.log',
                            filemode='a', format='%(asctime)s %(levelname)s - %(message)s', level=logging.INFO)
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    def start(self):
        try:
            logging.info('Scraper started. Checking shop: {}'.format(self.shop_name))
            self.shop_id = shop_helper.get_or_create_shop(self.shop_name, self.base_request_url)
            logging.info('Shop information gathered ID: {}. Starting scrapper.'.format(self.shop_id))
            self.scrapper_id = scrapper_helper.start_scrapper(self.shop_id)
            logging.info('Scrapper started ID: {}. Scrapping in progress.'.format(self.scrapper_id))
            self.scrap()
        except KeyboardInterrupt:
            logging.warning('Manually stopped.')
        except Exception as e:
            logging.error(e, exc_info=is_stacktrace_needed(e))

    def scrap(self):
        processing = True
        for search_url in self.search_urls:
            self.search_url = search_url
            while processing:
                logging.info('Gathering page HTML for page {}.'.format(self.page))
                self.get_page_html()
                logging.info('HTML fetched. Scrapping information.'.format(self.page))
                processing = self.scrap_page()

    def get_page_html(self):
        html = const.PAYLOAD not in self.config and self.get_simple_html() or self.get_payload_html()
        scrapper_helper.update_scrapper_page(self.page, self.scrapper_id)
        self.html = html

    def get_simple_html(self):
        return BeautifulSoup(crawler.simple_get(self.search_url % self.page), 'html.parser')

    def get_payload_html(self):
        return BeautifulSoup(
            json.loads(crawler.simple_post(
                self.search_url,
                self.config[const.PAYLOAD] % self.page
            ))['html'],
            'html.parser'
        )

    def scrap_page(self):
        html_product_list = self.select_html_product_list()
        if self.is_last_page(html_product_list):
            return False

        self.last_page_products = html_product_list
        for product_html in html_product_list:
            self.product_html = product_html
            logging.info('Scrapping product.')
            scrapped_product = self.scrap_product()
            if not scrapped_product:
                continue

            logging.info('Product scrapped. Sending to API.\n{}'.format(json_helper.dump(scrapped_product, 4)))
            self.persist_product(scrapped_product)
            logging.info('Successfully sent.')
        self.page += 1

        return True

    def is_last_page(self, html_product_list):
        if not html_product_list or html_product_list == self.last_page_products:
            logging.info('Last page reached. Stopping scrapper.')
            scrapper_helper.end_scrapper(self.scrapper_id)
            logging.info('Scrapper stopped.')
            return True

        return False

    def select_html_product_list(self):
        product_list = []
        for product in self.html.select(self.config[const.ROW]):
            product_list.append(product)
        return product_list

    def scrap_product(self):
        try:
            name = self.scrap_name()
            normal_price = self.scrap_normal_price()
            if not name or not normal_price:
                logging.warning('No name/price information. HTML dump:\n{}'.format(self.product_html))
                return None

            return Product(self.shop_id, name, normal_price, self.scrap_href(), self.scrap_discount_price(),
                           self.scrap_quality(), self.scrap_additional_info(), self.scrap_image())
        except Exception as e:
            logging.error('Exception: {}\nError at page: {}. HTML dump:\n{}'.format(e, self.page, self.product_html),
                          exc_info=True)
            return None

    def scrap_name(self):
        return fetch_helper.get_product_property(self.product_html, self.config[const.NAME])

    def scrap_normal_price(self):
        return fetch_helper.get_price_regex(
            fetch_helper.get_product_property(self.product_html, self.config[const.PRICE_NORMAL])
        )

    def scrap_href(self):
        href = fetch_helper.get_product_property(self.product_html, self.config[const.HREF])
        return href.startswith('http') \
               and href \
               or self.config[const.BASE_REQUEST_URL] + (href.startswith('/') and href[1:] or href)

    def scrap_discount_price(self):
        return fetch_helper.get_price_regex(
            fetch_helper.get_product_property(self.product_html, self.config[const.PRICE_DISCOUNT])
        )

    def scrap_quality(self):
        return fetch_helper.get_product_property(self.product_html, self.config[const.QUALITY])

    def scrap_additional_info(self):
        return fetch_helper.get_product_property(self.product_html, self.config[const.ADDITIONAL_INFO])

    def scrap_image(self):
        image_prefix = self.config[const.IMAGE][const.PREFIX] if const.PREFIX in self.config[const.IMAGE] else ''

        return image_helper.handle_image(
            self.shop_name,
            '{}{}'.format(image_prefix, fetch_helper.get_product_property(self.product_html, self.config[const.IMAGE]))
        )

    def persist_product(self, scrapped_product):
        product_helper.handle_product(scrapped_product)
        self.products_scrapped.append(scrapped_product)
        self.products_overall.append(self.products_scrapped[-1])


def is_stacktrace_needed(e):
    if isinstance(e, (Timeout, UnauthorizedException)):
        return False

    return True
