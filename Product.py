#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime


class Product:
    def __init__(self, shop_id, base_info, normal_price, link,
                 outlet_price=None, quality=None, additional_info=None, image=None
                 ):
        self.baseInfo = base_info
        if additional_info:
            self.additionalInfo = additional_info
        if normal_price:
            self.normalPrice = float(normal_price)
        if outlet_price:
            self.outletPrice = float(outlet_price)
        if quality:
            self.quality = quality
        if outlet_price and normal_price:
            self.discount = 1 - (float(outlet_price) / float(normal_price))
        self.image = image
        self.updatedAt = datetime.datetime.now().__str__()
        self.shop = shop_id
        self.link = link
